﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "VisualSettings", menuName = "Settings", order = 1)]
public class VisualSettings : ScriptableObject {

    public Sprite SpriteX {
        get {
            return _spriteX;
        }
    }

    public Sprite SpriteO {
        get {
            return _spriteO;
        }
    }

    public Color CellColorNormal {
        get {
            return _cellColorNormal;
        }
    }

    public Color CellColorWin {
        get {
            return _cellColorWin;
        }
    }

    public Color ColorX {
        get {
            return _colorX;
        }
    }

    public Color ColorO {
        get {
            return _colorO;
        }
    }

    [Header("Cell sprites")]
    [SerializeField]
    private Sprite _spriteX;

    [SerializeField]
    private Sprite _spriteO;

    [Header("Cell sprite colors")]
    [SerializeField]
    private Color _colorX = Color.black;

    [SerializeField]
    private Color _colorO = Color.black;

    [Header("Cell colors")]
    [SerializeField]
    private Color _cellColorNormal = Color.gray;

    [SerializeField]
    private Color _cellColorWin = Color.green;
}