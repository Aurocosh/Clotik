﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityWeld.Binding;

[Binding]
public class CellViewModel : BindableBase {

    [Binding]
    public Sprite CellSprite { get; set; }

    [Binding]
    public Color CellColor { get; set; }

    [Binding]
    public Color SpriteColor { get; set; }

    private Vector2Int _position;
    private GameManager _gameManager;
    private VisualSettings _visualSettings;

    public CellViewModel(CellState cellState, Vector2Int position, VisualSettings visualSettings) {
        SetValue(cellState);
        _position = position;
        _visualSettings = visualSettings;
        _gameManager = GameManager.Instance;

        CellColor = visualSettings.CellColorNormal;
    }

    public void SetValue(CellState cellState) {
        CellSprite = GetCellSprite(cellState);
        SpriteColor = GetSpriteColor(cellState);
    }

    private Sprite GetCellSprite(CellState cellState) {
        switch (cellState) {
            case CellState.X:
                return _visualSettings.SpriteX;

            case CellState.O:
                return _visualSettings.SpriteO;

            default:
                return null;
        }
    }

    private Color GetSpriteColor(CellState cellState) {
        switch (cellState) {
            case CellState.X:
                return _visualSettings.ColorX;

            case CellState.O:
                return _visualSettings.ColorO;

            default:
                return new Color(0, 0, 0, 0);
        }
    }

    [Binding]
    public void OnCellClicked() {
        _gameManager.OnCellClicked(_position);
    }

    public void SetCellLit(bool lit) {
        CellColor = lit ? _visualSettings.CellColorWin : _visualSettings.CellColorNormal;
    }
}