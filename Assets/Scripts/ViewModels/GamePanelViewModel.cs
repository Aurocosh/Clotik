﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityWeld.Binding;

[Binding]
public class GamePanelViewModel : MonoViewModel {

    [Binding]
    public Vector2 CellSize { get; set; }

    [Binding]
    public int ColumnCount { get; set; }

    [Binding]
    public ObservableList<CellViewModel> CellViewModels { get; private set; }

    [SerializeField]
    private float _fieldSize = 600;

    [SerializeField]
    private VisualSettings _visualSettings;

    private GameField _gameField;
    private CellViewModel[,] _cellViewModelGrid;

    private void Awake() {
        GameManager gameManager = GameManager.Instance;
        gameManager.VictoryDetected += OnVictoryDetected;

        int fieldSize = gameManager.FieldSize;
        _gameField = gameManager.GameField;
        _gameField.CellStateChanged += OnCellValueChanged;

        CellViewModels = new ObservableList<CellViewModel>();
        _cellViewModelGrid = new CellViewModel[fieldSize, fieldSize];

        for (int y = fieldSize - 1; y >= 0; y--) {
            for (int x = 0; x < fieldSize; x++) {
                var position = new Vector2Int(x, y);
                var value = _gameField[position];
                var cell = new CellViewModel(value, position, _visualSettings);
                CellViewModels.Add(cell);
                _cellViewModelGrid[x, y] = cell;
            }
        }

        ColumnCount = fieldSize;
        float cellSize = _fieldSize / ColumnCount;
        CellSize = new Vector2(cellSize, cellSize);
    }

    private void OnCellValueChanged(Vector2Int position, CellState cellState) {
        Assert.IsTrue(position.x >= 0);
        Assert.IsTrue(position.x < ColumnCount);
        Assert.IsTrue(position.y >= 0);
        Assert.IsTrue(position.y < ColumnCount);

        var cellViewModel = _cellViewModelGrid[position.x, position.y];
        cellViewModel.SetValue(cellState);
    }

    private void OnVictoryDetected(List<Vector2Int> positions) {
        foreach (Vector2Int position in positions) {
            var cellViewModel = _cellViewModelGrid[position.x, position.y];
            cellViewModel.SetCellLit(true);
        }
    }
}