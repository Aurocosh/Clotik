﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

[Binding]
public class VictoryTextViewModel : MonoViewModel {

    [Binding]
    public bool MessageVisible { get; set; }

    [Binding]
    public string VictoryMessage { get; set; }

    private void Awake() {
        GameManager.Instance.GameStateChanged += OnGameStateChanged;
        OnGameStateChanged(GameState.InProgress);
    }

    private void OnGameStateChanged(GameState gameState) {
        MessageVisible = gameState != GameState.InProgress;
        switch (gameState) {
            case GameState.FirstWon:
                VictoryMessage = "Player X won";
                break;

            case GameState.SecondWon:
                VictoryMessage = "Player O won";
                break;

            case GameState.Draw:
                VictoryMessage = "It's a draw!";
                break;

            default:
                VictoryMessage = "";
                break;
        }
    }
}