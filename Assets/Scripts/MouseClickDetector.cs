﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MouseClickDetector : MonoBehaviour, IPointerClickHandler {
    public UnityEvent MouseClickDetected;

    public void OnPointerClick(PointerEventData eventData) {
        MouseClickDetected.Invoke();
    }
}