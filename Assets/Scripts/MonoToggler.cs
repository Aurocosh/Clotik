﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoToggler : MonoBehaviour {

    public bool Toggled {
        get { return gameObject.activeSelf; }
        set {
            gameObject.SetActive(value);
        }
    }
}