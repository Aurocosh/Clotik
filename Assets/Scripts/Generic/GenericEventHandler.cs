﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventArgs<T> : EventArgs {
    public readonly T FirstValue;

    public EventArgs(T firstValue) {
        FirstValue = firstValue;
    }
}

public class DoubleEventArgs<TFirst, TSecond> : EventArgs {
    public readonly TFirst FirstValue;
    public readonly TSecond SecondValue;

    public DoubleEventArgs(TFirst firstValue, TSecond secondValue) {
        FirstValue = firstValue;
        SecondValue = secondValue;
    }
}

[SerializableAttribute]
public delegate void EventHandler<TSender, TEventArgs>(TSender sender, TEventArgs args) where TEventArgs : EventArgs;