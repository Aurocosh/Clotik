﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventExtensions {

    public static void Raise(this EventHandler eventToRaise, object sender) {
        if (eventToRaise != null)
            eventToRaise(sender, EventArgs.Empty);
    }

    public static void Raise<TSender, TEventArgs>(this EventHandler<TSender, TEventArgs> eventToRaise, TSender sender, TEventArgs args) where TEventArgs : EventArgs {
        if (eventToRaise != null)
            eventToRaise(sender, args);
    }

    public static void RaiseArg<TSender, TEventArgs, TFirst>(this EventHandler<TSender, TEventArgs> eventToRaise, TSender sender, TFirst firstArg)

        where TEventArgs : EventArgs<TFirst> {
        var args = new EventArgs<TFirst>(firstArg);

        if (eventToRaise != null)
            eventToRaise(sender, (TEventArgs)args);
    }

    public static void RaiseArg<TSender, TEventArgs, TFirst, TSecond>(this EventHandler<TSender, TEventArgs> eventToRaise, TSender sender, TFirst firstArg, TSecond secondArg)

    where TEventArgs : DoubleEventArgs<TFirst, TSecond> {
        var args = new DoubleEventArgs<TFirst, TSecond>(firstArg, secondArg);

        if (eventToRaise != null)
            eventToRaise(sender, (TEventArgs)args);
    }

    public static void Raise(this Action eventToRaise) {
        if (eventToRaise != null)
            eventToRaise();
    }

    public static void Raise<T>(this Action<T> eventToRaise, T arg) {
        if (eventToRaise != null)
            eventToRaise(arg);
    }

    public static void Raise<TFirst, TSecond>(this Action<TFirst, TSecond> eventToRaise, TFirst firstArg, TSecond secondArg) {
        if (eventToRaise != null)
            eventToRaise(firstArg, secondArg);
    }

    public static void Raise<TFirst, TSecond, TThird>(this Action<TFirst, TSecond, TThird> eventToRaise, TFirst firstArg, TSecond secondArg, TThird thirdArg) {
        if (eventToRaise != null)
            eventToRaise(firstArg, secondArg, thirdArg);
    }
}