﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

public abstract class BindableBase : INotifyPropertyChanged {
    public event PropertyChangedEventHandler PropertyChanged;
}