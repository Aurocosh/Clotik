﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GameField {
    public event Action<Vector2Int, CellState> CellStateChanged;

    public CellState this[Vector2Int vector2Int] {
        get {
            Assert.IsTrue(vector2Int.x >= 0);
            Assert.IsTrue(vector2Int.x < _fieldState.GetLength(0));
            Assert.IsTrue(vector2Int.y >= 0);
            Assert.IsTrue(vector2Int.y < _fieldState.GetLength(1));
            return _fieldState[vector2Int.x, vector2Int.y];
        }

        set {
            Assert.IsTrue(vector2Int.x >= 0);
            Assert.IsTrue(vector2Int.x < _fieldState.GetLength(0));
            Assert.IsTrue(vector2Int.y >= 0);
            Assert.IsTrue(vector2Int.y < _fieldState.GetLength(1));
            _fieldState[vector2Int.x, vector2Int.y] = value;
            CellStateChanged.Raise(vector2Int, value);
        }
    }

    public int FieldSize { get; private set; }
    public int WinningLineLength { get; set; }

    private CellState[,] _fieldState;

    public GameField(int fieldSize, int winningLineLength) {
        FieldSize = fieldSize;
        WinningLineLength = winningLineLength;
        _fieldState = new CellState[FieldSize, FieldSize];
        FillArray(_fieldState, CellState.Empty);
    }

    public bool InBounds(Vector2Int position) {
        if (position.x < 0)
            return false;
        if (position.y < 0)
            return false;
        if (position.x >= FieldSize)
            return false;
        if (position.y >= FieldSize)
            return false;
        return true;
    }

    private static void FillArray(CellState[,] array, CellState cellState) {
        for (int i = 0; i < array.GetLength(0); i++) {
            for (int j = 0; j < array.GetLength(1); j++)
                array[i, j] = cellState;
        }
    }
}