﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoSingleton<GameManager> {
    public event Action<GameState> GameStateChanged;
    public event Action<List<Vector2Int>> VictoryDetected;

    public int FieldSize {
        get {
            return _fieldSize;
        }
    }

    public int WinningLineSize {
        get {
            return _winningLineSize;
        }
    }

    public GameField GameField {
        get {
            return _gameField;
        }
    }

    public GameState GameState {
        get {
            return _gameState;
        }

        set {
            if (_gameState == value)
                return;
            _gameState = value;
            GameStateChanged.Raise(value);
        }
    }

    [SerializeField]
    private int _fieldSize = 3;

    [SerializeField]
    private int _winningLineSize = 3;

    private GameState _gameState;
    private GameField _gameField;
    private PlayerType _currentTurn;
    private int _moveCountX;
    private int _moveCountO;
    private int _cellCount;

    private List<Vector2Int> _directions;

    protected override void AwakeEx() {
        base.AwakeEx();
        _gameField = new GameField(_fieldSize, _winningLineSize);
        _directions = new List<Vector2Int> {
            new Vector2Int(0, 1),
            new Vector2Int(1, 0),
            new Vector2Int(0, -1),
            new Vector2Int(-1, 0),
            new Vector2Int(1, 1),
            new Vector2Int(1, -1),
            new Vector2Int(-1, -1),
            new Vector2Int(-1, 1)
        };
        GameState = GameState.InProgress;
        _moveCountX = 0;
        _moveCountO = 0;
        _cellCount = FieldSize * FieldSize;
    }

    public void OnCellClicked(Vector2Int position) {
        if (_gameField[position] != CellState.Empty)
            return;
        if (_gameState != GameState.InProgress)
            return;

        var currentState = _currentTurn == PlayerType.X ? CellState.X : CellState.O;
        _gameField[position] = currentState;

        if (_currentTurn == PlayerType.X)
            _moveCountX++;
        else if (_currentTurn == PlayerType.O)
            _moveCountO++;

        CheckAndSetGameState(position, currentState);

        _currentTurn = _currentTurn == PlayerType.X ? PlayerType.O : PlayerType.X;
    }

    private void CheckAndSetGameState(Vector2Int position, CellState currentState) {
        bool victory = DetectWin(position, currentState);
        if (victory)
            GameState = _currentTurn == PlayerType.X ? GameState.FirstWon : GameState.SecondWon;

        bool draw = DetectDraw();
        if (draw)
            GameState = GameState.Draw;
    }

    private bool DetectDraw() {
        return _moveCountX + _moveCountO == _cellCount && GameState == GameState.InProgress;
    }

    private bool DetectWin(Vector2Int position, CellState cellState) {
        foreach (Vector2Int direction in _directions) {
            Vector2Int lastPosition = position + direction * (WinningLineSize - 1);
            if (!_gameField.InBounds(lastPosition))
                continue;
            bool allMatch = true;
            var checkedPositions = new List<Vector2Int>();
            for (int i = 0; i < WinningLineSize; i++) {
                Vector2Int currentPosition = position + direction * i;
                allMatch = allMatch && _gameField[currentPosition] == cellState;
                checkedPositions.Add(currentPosition);
            }
            if (allMatch) {
                VictoryDetected.Raise(checkedPositions);
                return true;
            }
        }
        return false;
    }
}