﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T> {
    public static T Instance { get; protected set; }

    private void Awake() {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else {
            Instance = this as T;
            AwakeEx();
        }
    }

    protected virtual void AwakeEx() {
    }

    protected virtual void OnDestroyEx() {
    }

    void OnDestroy() {
        OnDestroyEx();
        if (Instance == this)
            Instance = null;
    }
}